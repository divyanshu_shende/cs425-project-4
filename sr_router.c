/**********************************************************************
 * file:  sr_router.c
 * date:  Mon Feb 18 12:50:42 PST 2002
 * Contact: casado@stanford.edu
 *
 * Description:
 *
 * This file contains all the functions that interact directly
 * with the routing table, as well as the main entry method
 * for routing.
 *
 **********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <time.h>
#include <stdint.h>

#include "sr_if.h"
#include "sr_rt.h"
#include "sr_router.h"
#include "sr_protocol.h"
#include "sr_arpcache.h"
#include "sr_utils.h"

/* TODO: Add constant definitions here... */

/* TODO: Add helper functions here... */
struct sr_if* matchif(struct sr_instance* sr, uint32_t tip)
{
    struct sr_if* if_walker = 0;
    if_walker = sr->if_list;

    while(if_walker)
    {
       if(if_walker->ip == tip)
        {
            return if_walker;
        }
        if_walker = if_walker->next;
    }
    return NULL;
}


/* See pseudo-code in sr_arpcache.h */
void handle_arpreq(struct sr_instance* sr, struct sr_arpreq *req)
{
  /* TODO: Fill this in */
  time_t curtime = time(NULL);
  if(difftime(curtime, req->sent) > 1.0)
  {
      /*
      if req->times_sent >= 5:
      */
      if(req->times_sent >= 5)
      {
          /*
          send icmp host unreachable to source addr of all pkts waiting
            on this request
          arpreq_destroy(req)
          */
          struct sr_packet *packlist = req->packets;
          while(packlist)
          {
              uint8_t *regret = malloc(packlist->len * sizeof(uint8_t));
              memcpy(regret, packlist->buf, packlist->len);
              struct sr_ethernet_hdr *regret_eth_hdr = (struct sr_ethernet_hdr *)(regret);
              struct sr_ethernet_hdr *packet_eth_hdr = (struct sr_ethernet_hdr *)(packlist->buf);
              memcpy(regret_eth_hdr->ether_dhost, packet_eth_hdr->ether_shost , ETHER_ADDR_LEN);
              memcpy(regret_eth_hdr->ether_shost, packet_eth_hdr->ether_dhost , ETHER_ADDR_LEN);

              struct sr_ip_hdr *regret_ip_hdr = (struct sr_ip_hdr *)(regret + sizeof(struct sr_ethernet_hdr));
              struct sr_ip_hdr *packet_ip_hdr = (struct sr_ip_hdr *)(packlist->buf + sizeof(struct sr_ethernet_hdr));
              regret_ip_hdr->ip_src = packet_ip_hdr->ip_dst;
              regret_ip_hdr->ip_dst = packet_ip_hdr->ip_src;

              struct sr_icmp_hdr *regret_icmp_hdr = (struct sr_icmp_hdr *)(regret + sizeof(struct sr_ethernet_hdr)
                                                                                  + sizeof(struct sr_ip_hdr));
              regret_icmp_hdr->icmp_type = 3;
              regret_icmp_hdr->icmp_code = 1;

              sr_send_packet(sr, packlist->buf, packlist->len, packlist->iface);
              free(regret);
              packlist = packlist->next;
          }
          sr_arpreq_destroy(&sr->cache, req);
      }
      else
      {
          /*send arp request*/
          struct sr_if* if_walker = 0;
          if_walker = sr->if_list;
          while(if_walker)
          {
              unsigned int len = sizeof(struct sr_ethernet_hdr) + sizeof(struct sr_arp_hdr);
              uint8_t *myreq = malloc(len * sizeof(uint8_t));

              struct sr_ethernet_hdr *req_eth = (struct sr_ethernet_hdr *)(myreq);
              memcpy(req_eth->ether_shost, if_walker->addr, ETHER_ADDR_LEN);
              memset(req_eth->ether_dhost, 255, ETHER_ADDR_LEN); /* Broadcast! */
              req_eth->ether_type = htons(ethertype_arp);

              struct sr_arp_hdr *req_arp = (struct sr_arp_hdr *)(myreq + sizeof(struct sr_ethernet_hdr));
              req_arp->ar_hrd = htons(1);
              req_arp->ar_pro = htons(0x0800);
              req_arp -> ar_hln = 6;
              req_arp->ar_pln = 4;
              req_arp->ar_op = htons(arp_op_request);
              req_arp->ar_sip = if_walker->ip;
              req_arp->ar_tip = req->ip;
              memcpy(req_arp->ar_sha, if_walker->addr, ETHER_ADDR_LEN);
              sr_send_packet(sr, myreq, len, if_walker->name);
              if_walker = if_walker->next;
          }
          req->sent = curtime;
          req->times_sent++;
      }
  }
}

/*---------------------------------------------------------------------
 * Method: sr_init(void)
 * Scope:  Global
 *
 * Initialize the routing subsystem
 *
 *---------------------------------------------------------------------*/

void sr_init(struct sr_instance* sr)
{
    /* REQUIRES */
    assert(sr);

    /* Initialize cache and cache cleanup thread */
    sr_arpcache_init(&(sr->cache));

    pthread_attr_init(&(sr->attr));
    pthread_attr_setdetachstate(&(sr->attr), PTHREAD_CREATE_JOINABLE);
    pthread_attr_setscope(&(sr->attr), PTHREAD_SCOPE_SYSTEM);
    pthread_attr_setscope(&(sr->attr), PTHREAD_SCOPE_SYSTEM);
    pthread_t thread;

    pthread_create(&thread, &(sr->attr), sr_arpcache_timeout, sr);

    /* TODO: (opt) Add initialization code here */

} /* -- sr_init -- */

/*---------------------------------------------------------------------
 * Method: sr_handlepacket(uint8_t* p,char* interface)
 * Scope:  Global
 *
 * This method is called each time the router receives a packet on the
 * interface.  The packet buffer, the packet length and the receiving
 * interface are passed in as parameters. The packet is complete with
 * ethernet headers.
 *
 * Note: Both the packet buffer and the character's memory are handled
 * by sr_vns_comm.c that means do NOT free either (signified by "lent" comment).
 * Make a copy of the
 * packet instead if you intend to keep it around beyond the scope of
 * the method call.
 *
 *---------------------------------------------------------------------*/

void sr_handlepacket(struct sr_instance* sr,
        uint8_t * packet/* lent */,
        unsigned int len,
        char* interface/* lent */){

  /* REQUIRES */
  assert(sr);
  assert(packet);
  assert(interface);

  struct sr_ethernet_hdr *Frame = (struct sr_ethernet_hdr *)(packet);
  /*print_hdr_eth(packet);*/
  /*print_hdr_arp(packet);*/
  /*uint16_t Frame->ether_type = ntohs();*/
  if(ntohs(Frame->ether_type) == ethertype_arp)
  {
      /*You have an ARP packet!*/
      printf("ARP packet!\n");
      /*struct sr_arpreq *sr_arpcache_queuereq(sr->cache, ip, packet, packet_len, interface);*/
      struct sr_arp_hdr *arphdr = (struct sr_arp_hdr *)(packet + sizeof(struct sr_ethernet_hdr));
      struct sr_if *thisif = sr_get_interface(sr, interface);
      /*Is it a request or a response?*/
      if(ntohs(arphdr->ar_op) == arp_op_request)
      {
          /* Case : ARP request*/
          printf("Case : ARP request\n");
          /*print_hdrs(packet, len);*/
          /*print_hdr_arp(arphdr);*/
          /*printf("IP asked = %u\n", (arphdr->ar_tip));*/
          /*printf("interface = %s\n", thisif->name);
          printf("My IP = %u\n", thisif->ip);*/
          if(arphdr->ar_tip == thisif->ip)
          {
              /*printf("It's asking for my address! \n");*/
              struct sr_if *asked_if = matchif(sr, arphdr->ar_tip);
              /*printf("It asked for %s\n", asked_if->name);*/
              /* Send ARP reply */
              uint8_t *reply = malloc(len * sizeof(uint8_t));
              memcpy(reply, packet, len);
              struct sr_ethernet_hdr *reply_eth_hdr = (struct sr_ethernet_hdr *)(reply);
              /* Set headers! */
              /*printf("Setting ethernet headers!\n");*/
              memcpy(reply_eth_hdr->ether_shost, thisif->addr, ETHER_ADDR_LEN);
              memcpy(reply_eth_hdr->ether_dhost, Frame->ether_shost, ETHER_ADDR_LEN);
              /* Now set ARP headers! */
              /*printf("Setting ARP headers!\n");*/
              /*print_hdr_eth(reply);*/
              struct sr_arp_hdr *reply_arphdr = (struct sr_arp_hdr *)(reply + sizeof(struct sr_ethernet_hdr));
              reply_arphdr->ar_tip = arphdr->ar_sip;
              memcpy(reply_arphdr->ar_tha, arphdr->ar_sha, ETHER_ADDR_LEN);
              /*printf("targets set\n");*/
              reply_arphdr->ar_op = htons(arp_op_reply);
              /*printf("I hope it's not this thing!\n");*/
              reply_arphdr->ar_sip = arphdr->ar_tip;
              /*printf("I sure sure sure hope it's not this thing!\n");*/
              memcpy(reply_arphdr->ar_sha, asked_if->addr, ETHER_ADDR_LEN);
              /*printf("source set\n");
              printf("Set ARP headers!\n");*/
              /*print_hdr_eth(reply);*/
              print_hdrs(reply, len);
              printf("Sending packet!\n");
              sr_send_packet(sr, reply, len, interface);
              /*printf("This got sent!\n");*/
          }
          else
          {
              /* Probably Forwarding to next-hop IP. */
              /* printf("Wrong number!\n"); */
          }
      }
      else if(ntohs(arphdr->ar_op) == arp_op_reply)
      {
          /*Case : ARP response*/
          printf("Case : ARP response\n");
          /*print_hdrs(packet, len);*/
          /*print_hdr_arp((uint8_t*)arphdr);*/
          /*Add IP->MAC mapping to table*/
          struct sr_if* if_walker = 0;
          if_walker = sr->if_list;
          while(if_walker)
          {
              if(if_walker->ip == arphdr->ar_tip)
              {
                  break;
              }
              if_walker = if_walker->next;
          }
          if(if_walker)
          {
              struct sr_arpreq *addresp = sr_arpcache_insert(&(sr->cache), arphdr->ar_tha, arphdr->ar_tip);
              struct sr_packet *packlist = addresp->packets;
              while(packlist)
              {
                  struct sr_ethernet_hdr *eth_hdr = (struct sr_ethernet_hdr *)(packlist->buf);
                  struct sr_if *send_if = sr_get_interface(sr, packlist->iface);
                  memcpy(eth_hdr->ether_dhost, arphdr->ar_sha, 6);
                  sr_send_packet(sr, packlist->buf, packlist->len, send_if->name);
                  packlist = packlist->next;
              }
          }
          else
          {
              printf("If_walket NULL error!\n");
          }
      }
      else
      {
          printf("Something weird happened!\n");
      }
  }
  else if(ntohs(Frame->ether_type) == ethertype_ip)
  {
      printf("IP packet!\n");
      struct sr_ip_hdr *iphdr = (struct sr_ip_hdr *)(packet + sizeof(struct sr_ethernet_hdr));
      struct sr_if* if_walker = 0;
      if_walker = sr->if_list;
      while(if_walker)
      {
          if(if_walker->ip == iphdr->ip_dst)
          {
              break;
          }
          if_walker = if_walker->next;
      }
      if(if_walker != NULL)
      {
          /* Ping to one of our interfaces! */
          /*struct sr_if *thisif = sr_get_interface(sr, interface);*/
          if(iphdr->ip_p == ip_protocol_icmp)
          {
              /* ICMP request! */
              struct sr_icmp_hdr *icmphdr = (struct sr_icmp_hdr *)(packet + sizeof(struct sr_ethernet_hdr)
                                                                            + sizeof(struct sr_ip_hdr));
              if(icmphdr->icmp_type == 8)
              {
                  /* Send icmp response. */
                  printf("Sending ICMP response!\n");
                  uint8_t *reply = malloc(len * sizeof(uint8_t));
                  memcpy(reply, packet, len);
                  struct sr_ethernet_hdr *reply_eth_hdr = (struct sr_ethernet_hdr *)(reply);
                  memcpy(reply_eth_hdr->ether_shost, Frame->ether_dhost, ETHER_ADDR_LEN);
                  memcpy(reply_eth_hdr->ether_dhost, Frame->ether_shost, ETHER_ADDR_LEN);

                  struct sr_ip_hdr *reply_ip_hdr = (struct sr_ip_hdr *)(reply + sizeof(struct sr_ethernet_hdr));
                  reply_ip_hdr->ip_src = iphdr->ip_dst;
                  reply_ip_hdr->ip_dst = iphdr->ip_src;

                  struct sr_icmp_hdr *reply_icmp_hdr = (struct sr_icmp_hdr *)(reply + sizeof(struct sr_ethernet_hdr)
                                                                                        + sizeof(struct sr_ip_hdr));
                  reply_icmp_hdr->icmp_type = 0;
                  sr_send_packet(sr, reply, len, interface);
                  printf("ICMP sent!\n");
              }
          }
      }
  }
  else
  {
      printf("You are nowhere!\n");
  }
  printf("*** -> Received packet of length %d\n",len);

  /* TODO: Add forwarding logic here */



}/* -- sr_handlepacket -- */
